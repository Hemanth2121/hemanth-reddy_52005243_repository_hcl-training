package factory;

public class Category {
	// Based on user selection from menu ,Object will created
	public static Classification setMovieType(int type) {

		if(type==1) {
			return new CommingSoon();
		}
		else if(type==2) {
			return new MoviesInTheaters();
		}
		else if(type==3) {
			return new TopRatedIndian();
		}
		else if(type==4) {
			return new TopRatedMovies();
		}
		else {
			return null;
		}
	}
}
