package main;


import java.sql.SQLException;
import java.util.List;
import java.util.Scanner;


import factory.Category;
import factory.Classification;
import model.Movie;


public class TestMain {

	public static void main(String[] args) {
		int choice;


		Scanner sc = new Scanner(System.in);
		do
		{

			System.out.println("Welcome");
			System.out.println("Please select a category");
			System.out.println(
					"1)Upcoming movies\n"
							+ "2)Movies In Theaters\n"
							+ "3)Top Rated Indian Movies\n"
							+ "4)Top Rated Movies\n"
							+ "5) to exit");
			System.out.println("Enter your choice : ");
			System.out.println();

			choice = sc.nextInt();

			switch(choice) {
			case 1:
				Classification cs=Category.setMovieType(1);
				try {
					List<Movie> movie=cs.movieType();
					System.out.println("======Upcoming Movies======");
					for(Movie mo:movie) {
						System.out.println(mo+ "\n");
					}
				}catch(SQLException e) {
					e.printStackTrace();
				}
				break;
			case 2:
				Classification mit=Category.setMovieType(2);
				try {
					List<Movie> movie=mit.movieType();
					System.out.println("======Movies in Theaters======");
					for(Movie mo:movie) {
						System.out.println(mo+ "\n");
					}
				}catch(SQLException e) {
					e.printStackTrace();
				}
				break;
			case 3:
				Classification tri=Category.setMovieType(3);
				try {
					List<Movie> movie=tri.movieType();
					System.out.println("======Top Rated Indian Movies======");
					for(Movie mo:movie) {
						System.out.println(mo+ "\n");
					}
				}catch(SQLException e) {
					e.printStackTrace();
				}
				break;
			case 4:
				Classification trm=Category.setMovieType(4);
				try {
					List<Movie> movie=trm.movieType();
					System.out.println("======Top Rated Movies======");
					for(Movie mo:movie) {
						System.out.println(mo+ "\n");
					}
				}catch(SQLException e) {
					e.printStackTrace();
				}
				break;

			case 5:
				System.exit(0);
				break;
			default:
				System.out.println("Invalid Choice");
				break;
			}
		}while(choice!=5);
	}

}
